Source: minia
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Olivier Sallou <osallou@debian.org>,
           Andreas Tille <tille@debian.org>,
           Steffen Moeller <moeller@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               cmake,
               bc,
               zlib1g-dev,
               libboost-dev,
               libgatbcore-dev,
               libhdf5-dev
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/minia
Vcs-Git: https://salsa.debian.org/med-team/minia.git
Homepage: http://minia.genouest.org/
Rules-Requires-Root: no

Package: minia
Architecture: any-amd64 arm64 loong64 mips64el ppc64el ia64 ppc64 riscv64 sparc64 alpha
Depends: ${misc:Depends},
         ${shlibs:Depends},
         zlib1g,
         bc
Recommends: samtools
Suggests: bandage
Description: short-read biological sequence assembler
 What was referred to as "next-generation" DNA sequencing up to
 the year 2020 delivered only "short" reads up to ~600 base pairs
 in length that would then have to be puzzled by random overlaps
 in their sequence towards a complete genome. This is the genome
 assembly. And there are many biological pitfalls on long stretches
 of low complexity regions and copy number variations and other
 sorts of redundancies that render this difficult.
 .
 This package provides a short-read DNA sequence assembler based on a
 de Bruijn graph, capable of assembling a human genome on a desktop
 computer in a day.
 .
 The output of Minia is a set of contigs, i.e. stretches of gap-free
 linear overlaps of short reads. In the best possible case this is
 a whole chromosome.
 .
 Minia produces results of similar contiguity and accuracy to other
 de Bruijn assemblers (e.g. Velvet).
